CFLAGS+= -Wall -I/usr/include/kqueue -I. -fPIC -g
CFLAGS+= -include bsd/sys/time.h
LDFLAGS+= -lkqueue -L. -lbsd -lhashtab
SRCS=$(wildcard *.c)
OBJS=$(patsubst %.c, %.o, $(SRCS))
all: libdht.a libdht.so
%.o: %.c 
	$(CC) -o $@ -c $< $(CFLAGS)
libdht.a: $(OBJS)
	ar cq libdht.a murmur3.o buffer.o dht.o
libdht.so: $(OBJS)
	cc -shared -o libdht.so murmur3.o buffer.o dht.o
install: libdht.a libdht.so
	install -m 0644 libdht.a /usr/lib/
	install -m 0644 libdht.so /usr/lib/
	install -m 0644 dht.h /usr/include/
clean:
	rm -f *.o *.a *.so
