/*
 * Copyright (c) 2016 Mohamed Aslan <maslan@sce.carleton.ca>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/event.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <unistd.h>
#include <arpa/inet.h>

#include <dht.h>
#include <murmur3.h>
#include <hashtab.h>

#define TCPBACKLOG		500

#define POSTINIT_WAIT		0
#define POSTINIT_TID		0

#define MAX_PEERS		64

#include <stdio.h>


static int
ht_init(void **ht)
{
	struct hashtab		*hts;

	if (ht == NULL)
		return 0;
	/* allocate two hashtabales one for data and one for timestamps */
	hts = (struct hashtab *)calloc(2, sizeof(struct hashtab));
	if (!hashtab_init(&hts[0], 16, NULL))
		return 0;
	if (!hashtab_init(&hts[1], 16, NULL))
		return 0;
	*ht = (void *)(hts);
	return 1;
}

static int
ht_put(void *ht, const char *key, const char *val, struct timespec *ts)
{
	struct hashtab		*hts = (struct hashtab *)ht;

	if (!hashtab_put(&hts[0], (void *)key, strlen(key) + 1, (void *)val, strlen(val) + 1))
		return 0;
	if (!hashtab_put(&hts[1], (void *)key, strlen(key) + 1, (void *)ts, sizeof(struct timespec)))
		return 0;
	return 1;
}

static int
ht_get(void *ht, const char *key, char **val, struct timespec **ts)
{
	size_t len;
	struct hashtab		*hts = (struct hashtab *)ht;

	if (!hashtab_get(&hts[0], (void *)key, strlen(key) + 1, (void **)val, &len))
		return 0;
	if (!hashtab_get(&hts[1], (void *)key, strlen(key) + 1, (void **)ts, &len))
		return 0;
	return 1;
}

static int
ht_del(void *ht, const char *key)
{
	struct hashtab		*hts = (struct hashtab *)ht;

	if (!hashtab_del(&hts[0], (void *)key, strlen(key) + 1))
		return 0;
	if (!hashtab_del(&hts[1], (void *)key, strlen(key) + 1))
		return 0;
	return 1;
}

static uint64_t
murmur3_partitioner(const char *key)
{
	uint64_t token[2];

	murmurhash3_x64_128(key, strlen(key), 0, token);
	return token[0];
}

static struct dht_options defopts = {
	.partitioner = murmur3_partitioner,
	.ht_init = ht_init,
	.ht_put = ht_put,
	.ht_get = ht_get,
	.ht_del = ht_del
};

/*
 * - assume array is already sorted
 * - O(n)
 */
static int
insert_token_sorted(struct dht_peer peers[], int *n, uint64_t token)
{
	int i, j;

	for (i = 0 ; i < *n ; i++) {
		if (peers[i].token > token)
			break;
	}
	for (j = *n ; j > i ; j--) {
		peers[j] = peers[j - 1];
	}
	peers[i].token = token;
	++*n;
	return i;
}

/*
 * - slightly modified binary search with wrapping range
 * - O(log n)
 */
static int
find_coordinator(struct dht_peer peers[], int n, uint64_t token)
{
	int min = 0, max = n - 1, mid = 0;

	if (peers[max].token < token)
		return min;
	while (min <= max) {
		mid = min + ((max - min) / 2);
		if (peers[mid].token == token)
			return mid;
		if (peers[mid].token < token)
			min = mid + 1;
		else
			max = mid - 1;
	}
#ifdef DEBUG
	printf("min = %d, mid = %d, max = %d\n", min, mid, max);
#endif
	return min;
}

#ifdef DEBUG
static void
print(struct dht_peer peers[], int n)
{
	int i;

	for (i = 0 ; i < n ; i++)
		printf("0x%llx ", peers[i].token);
	printf("\n");
}
#endif

int
dht_init(struct dht_node *node, const char *id, int port, int n, uint32_t flags, struct dht_options *opts)
{
	int s_type;
	struct kevent kev;

	if (node == NULL || id == NULL)
		return 0;
	node->id = strdup(id);
	node->port = port;
	node->n_replicas = n;
	node->flags = flags;
	if (opts == NULL)
		node->opts = &defopts;
	if ((node->ev_queue = kqueue()) == -1)
		return 0;
	if (!(node->flags & DHT_USEUDP))	/* TCP */
		s_type = SOCK_STREAM;
	else	/* UDP */
		s_type = SOCK_DGRAM;
	if ((node->socket = socket(AF_INET, s_type, 0)) < 0)
		return 0;
	/* for now assume ipv4 */
	memset((void *)&(node->saddr), 0, sizeof(struct sockaddr_in));
	((struct sockaddr_in *)&(node->saddr))->sin_family = AF_INET;
	((struct sockaddr_in *)&(node->saddr))->sin_port = htons(node->port);
	((struct sockaddr_in *)&(node->saddr))->sin_addr.s_addr = htonl(INADDR_ANY);
	if (bind(node->socket, (struct sockaddr *)&(node->saddr), sizeof(struct sockaddr)) == -1)
		goto error;
	if (!(node->flags & DHT_USEUDP)) 
		if (listen(node->socket, TCPBACKLOG) == -1)
			goto error;
	EV_SET(&kev, POSTINIT_TID, EVFILT_TIMER, EV_ADD | EV_ENABLE | EV_ONESHOT, 0, POSTINIT_WAIT, NULL);
	if ((kevent(node->ev_queue, &kev, 1, NULL, 0, NULL)) == -1)
		goto error;
	EV_SET(&kev, node->socket, EVFILT_READ, EV_ADD, 0, 0, NULL);
	if ((kevent(node->ev_queue, &kev, 1, NULL, 0, NULL)) == -1)
		goto error;
	node->peers = reallocarray(NULL, MAX_PEERS, sizeof(struct dht_peer));
	node->token = node->opts->partitioner(node->id);
	node->n_peers = 0;
	insert_token_sorted(node->peers, &node->n_peers, node->token);
	if (!node->opts->ht_init(&node->ht))
		goto error;
	node->ready = 1;
	return 1;
error:
	close(node->socket);
	return 0;
}

void
dht_add_peer(struct dht_node *node, const char *id, const char *ip, int port)
{
	uint64_t token;
	int i, s_type;

	if (node == NULL || id == NULL || ip == NULL)
		return;
	if (node->n_peers == MAX_PEERS)
		return;
	token = node->opts->partitioner(id);
	i = insert_token_sorted(node->peers, &node->n_peers, token);
	node->peers[i].id = strdup(id);
	node->peers[i].last_recv = 0;
	node->peers[i].ready = 0;
#ifdef DEBUG
	print(node->peers, node->n_peers);
#endif
	if (!(node->flags & DHT_USEUDP))	/* TCP */
		s_type = SOCK_STREAM;
	else	/* UDP */
		s_type = SOCK_DGRAM;
	if ((node->peers[i].socket = socket(AF_INET, s_type, 0)) < 0)
		return;
	/* for now assume ipv4 */
	memset((void *)&(node->peers[i].s_addr), 0, sizeof(struct sockaddr_in));
	((struct sockaddr_in *)&(node->peers[i].s_addr))->sin_family = AF_INET;
	((struct sockaddr_in *)&(node->peers[i].s_addr))->sin_port = htons(port);
	((struct sockaddr_in *)&(node->peers[i].s_addr))->sin_addr.s_addr = inet_addr(ip);
}

static void
post_init(struct dht_node *node)
{
	int i;

	if (!(node->flags & DHT_USEUDP)) {	/* TCP */
		for (i = 0 ; i < node->n_peers ; i++) {
			if (node->token == node->peers[i].token)	/* do not connect to oneself */
				continue;
			if (connect(node->peers[i].socket, (struct sockaddr *)&node->peers[i].s_addr, sizeof(struct sockaddr)) < 0) {
				printf("[%s] warning connection error\n", node->id);
			} else {
				node->peers[i].ready = 1;
			}
		}
	}
}


void
dht_event_loop(struct dht_node *node)
{
	char *vp;
	char buf[BUFSIZ + 1];
	char cmd[BUFSIZ + 1], key[BUFSIZ + 1], val[BUFSIZ + 1];
	int n, s;
	int cl;
	struct kevent kev;
	struct sockaddr_storage sw_addr;
	struct timespec ts, *tsp;
	socklen_t addr_len = sizeof(struct sockaddr_storage);
	ssize_t bytes;

	if (node == NULL)
		return;

	while (1) {
		n = kevent(node->ev_queue, NULL, 0, &kev, 1, NULL);	/* TODO: use bigger eventlist */
		if (n < 0)
			break;
		if (n == 0)
			continue;
		if (kev.filter == EVFILT_TIMER) {
			s = (int)(kev.ident);	/* uintptr_t != int */
			if (s == POSTINIT_TID)
				post_init(node);
		} else if (kev.filter == EVFILT_READ) {
			s = (int)(kev.ident);	/* uintptr_t != int */
			if ((s == node->socket) && !(node->flags & DHT_USEUDP)) {	/* new connection */
				cl = accept(s, (struct sockaddr *)&sw_addr, &addr_len);
				EV_SET(&kev, cl, EVFILT_READ, EV_ADD | EV_CLEAR, 0, 0, NULL);
				if ((kevent(node->ev_queue, &kev, 1, NULL, 0, NULL)) == -1)
					break;
			} else {	/* data received */
				if (!(node->flags & DHT_USEUDP)) {	/* TCP */
					bytes = read(s, buf, sizeof(buf));
					if (bytes == 0) {
						EV_SET(&kev, s, EVFILT_READ, EV_DELETE, 0, 0, NULL);
						if ((kevent(node->ev_queue, &kev, 1, NULL, 0, NULL)) == -1)
							break;
						printf("[%s] peer disconnected.\n", node->id);
					} else {
#ifdef DEBUG
						printf("[%s] read %zd bytes.\n", node->id, bytes);
#endif
						/* TODO: implement a proper parser */
						sscanf(buf, "%s", cmd);
						if (!strncmp(cmd, "PUT", sizeof(cmd))) {
							sscanf(buf + 4, "%s %lld.%ld %s", key, &ts.tv_sec, &ts.tv_nsec, val);	/* unsafe */
							node->opts->ht_put(node->ht, key, val, &ts);
						}
						else if (!strncmp(cmd, "GET", sizeof(cmd))) {
							sscanf(buf + 4, "%s", key);		/* unsafe */
							if (node->opts->ht_get(node->ht, key, &vp, &tsp)) {
								snprintf(val, sizeof(val), "%lld.%.9ld %s\n", ts.tv_sec, ts.tv_nsec, vp);
								write(s, val, strlen(val));
							}
							/* XXX: else */
						}
						else {
							printf("[%s] unknown command: %s.\n", node->id, cmd);
						}
					}
				} else {	/* UDP */
					/* XXX: NOT YET IMPLEMENTED */
					bytes = recvfrom(s, buf, BUFSIZ, 0, (struct sockaddr *)&sw_addr, &addr_len);
					if (bytes == 0)
						break;
#ifdef DEBUG
					else
						printf("read %zd bytes.\n", bytes);
#endif
				}
			}
		}
	}
}

int
dht_put_tunable(struct dht_node *node, const char *key, const char *val, struct timespec *ts, int w)
{
	char *msg;
	int coord, i = 0, replica, success = 0;
	uint64_t token;

	if (node == NULL || key == NULL || val == NULL || ts == NULL)
		return 0;
	asprintf(&msg, "PUT %s %lld.%.9ld %s\n", key, ts->tv_sec, ts->tv_nsec, val);

	token = murmur3_partitioner(key);
	coord = find_coordinator(node->peers, node->n_peers, token);
#ifdef DEBUG
	printf("[%s] coord for \"%s\" 0x%llx is 0x%llx\n", node->id, key, token, node->peers[coord].token);
#endif

	while (i < node->n_replicas) {
		replica = (coord + i++) % node->n_peers;
#ifdef DEBUG
		printf("[%s] replica: %d\n", node->id, replica);
#endif
		if (node->peers[replica].token == node->token) {
			if (node->opts->ht_put(node->ht, key, val, ts))
				success++;
			continue;
		}
		if (!node->peers[replica].ready)
			continue;
		if (!(node->flags & DHT_USEUDP)) {	/* TCP */
			if (write(node->peers[replica].socket, msg, strlen(msg)) > 0)
				success++;
		} else {	/* UDP */
			if (sendto(node->peers[replica].socket, msg, strlen(msg), 0, (struct sockaddr *)&(node->peers[replica].s_addr), sizeof(struct sockaddr)) > 0)
				success++;
		}
	}
	free(msg);
#ifdef DEBUG
	printf("[%s] success: %d\n", node->id, success);
#endif
	if (success >= w)
		return 1;
	return 0;
}

int
dht_get_tunable(struct dht_node *node, const char *key, char **val, int r)
{
	char *msg, *tmpval, *recent_val = NULL;
	char buf[BUFSIZ + 1], vbuf[BUFSIZ + 1];
	int coord, i = 0, replica, success = 0;
	struct timespec recent_ts, ts, *tsp;
	ssize_t bytes;
	uint64_t token;

	if (node == NULL || key == NULL || val == NULL)
		return 0;
	timespecclear(&recent_ts);
	asprintf(&msg, "GET %s\n", key);

	token = murmur3_partitioner(key);
	coord = find_coordinator(node->peers, node->n_peers, token);
#ifdef DEBUG
	printf("[%s] coord for \"%s\" 0x%llx is 0x%llx\n", node->id, key, token, node->peers[coord].token);
#endif

	while (i < node->n_replicas) {
		replica = (coord + i++) % node->n_peers;
#ifdef DEBUG
		printf("[%s] replica: %d\n", node->id, replica);
#endif
		if (node->peers[replica].token == node->token) {
			if (node->opts->ht_get(node->ht, key, &tmpval, &tsp)) {
				if (timespeccmp(tsp, &recent_ts, >=)) {
					recent_ts = *tsp;
					if (recent_val)
						free(recent_val);
					asprintf(&recent_val, "%s", tmpval);
				}
				success++;
			}
			continue;
		}
		if (!node->peers[replica].ready)
			continue;
		if (!(node->flags & DHT_USEUDP)) {	/* TCP */
			bytes = write(node->peers[replica].socket, msg, strlen(msg));
			if (bytes <= 0)
				continue;
#ifdef DEBUG
			printf("[%s] message sent to peer [0x%llx].\n", node->id, node->peers[replica].token);
#endif
			bytes = read(node->peers[replica].socket, buf, sizeof(buf));
#ifdef DEBUG
			printf("[%s] reponse is %zu bytes\n", node->id, bytes);
#endif
			if (bytes <= 0)
				continue;
			buf[bytes - 1] = 0;
			sscanf(buf, "%lld.%ld %s", &ts.tv_sec, &ts.tv_nsec, vbuf);
#ifdef DEBUG
			printf("[%s] GOT val: %s for key: %s.\n", node->id, vbuf, key);
#endif
			if (timespeccmp(&ts, &recent_ts, >=)) {
				recent_ts = ts;
				if (recent_val)
					free(recent_val);
				asprintf(&recent_val, "%s", vbuf);
			}
			success++;
		} else {	/* UDP */
			/* XXX: NOT YET IMPLEMENTED */
			if (sendto(node->peers[replica].socket, msg, strlen(msg), 0, (struct sockaddr *)&(node->peers[replica].s_addr), sizeof(struct sockaddr)) > 0)
				success++;
		}
	}
	if (success)
		*val = recent_val;
	free(msg);
#ifdef DEBUG
	printf("[%s] success: %d\n", node->id, success);
#endif
	if (success >= r)
		return 1;
	return 0;
}
