/*
 * Copyright (c) 2017 Mohamed Aslan <maslan@sce.carleton.ca>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <dht.h>
#include <hashtab.h>
#include <pthread.h>
#include <err.h>
#include <errno.h>
#include <unistd.h>
#include <sys/wait.h>

#define	N_REPLICAS	2

#define	MAX_KEYS	5
struct v_node {
	char *id;
	char *ip;
	int port;
	int n_keys;
	char *keys[MAX_KEYS];
	char *vals[MAX_KEYS];
};

#define	N_NODES	3
static struct v_node vnodes[] = {
	{"A", "127.0.0.1", 5000, 5,
	    {"hello", "1hello", "12hello", "world1", "world12"},
	    {"world", "world", "world", "hello", "hello"},
	},
	{"B", "127.0.0.1", 5001, 3,
	    {"1hello", "world", "world1", NULL, NULL},
	    {"world", "hello", "hello", NULL, NULL},
	},
	{"C", "127.0.0.1", 5002, 4,
	    {"hello", "12hello", "world", "world12", NULL},
	    {"world", "world", "hello", "hello", NULL},
	}
};

#define	N_KV	6
char *keys[N_KV] = {
	"hello", "world", "1hello",
	"world1", "12hello", "world12"
	};
char *vals[N_KV] = {
	"world", "hello", "world",
	"hello", "world", "hello"
	};

struct args {
	int nidx;
	struct dht_node *node;
};


void *
test_thread(void *arg)
{
	int i, fail = 0;
	struct dht_node *n = ((struct args *)arg)->node;
	char *val;

	while (!n->ready);

	sleep(5);
	/* read all keys/vals */
	for (i = 0 ; i < N_KV ; i++) {
		if (!dht_get_tunable(n, keys[i], &val, N_REPLICAS)) {
			printf("FAILED [%s] dht_get_tunable.\n", n->id);
			printf("for %s:%s\n", keys[i], vals[i]);
			fail++;
			continue;
		}
		if (strcmp(vals[i], val)) {
			printf("FAILED [%s] dht_get_tunable, received bad val.\n", n->id);
			fail++;
		}
	}
	printf("OK\n");
	sleep(5);

	_exit(fail);
	/* NOTREACHED */
	return NULL;
}

int
node(int me)
{
	int i;
	struct dht_node *n;
	struct args *a;
	struct hashtab *ht;
	struct timespec ts;
	pthread_t tp, tt;

	n = malloc(sizeof(struct dht_node));
	if (!dht_init(n, vnodes[me].id, vnodes[me].port, N_REPLICAS, 0, NULL))
		err(1, "dht_init");

	/* populate hashtable */
	ht = (struct hashtab *)n->ht;
	(void)clock_gettime(CLOCK_REALTIME, &ts);
	for (i = 0 ; i < vnodes[me].n_keys ; i++) {
		n->opts->ht_put(ht, vnodes[me].keys[i], vnodes[me].vals[i], &ts);
	}

	for (i = 0 ; i < N_NODES ; i++)
		if (i != me)
			dht_add_peer(n, vnodes[i].id, vnodes[i].ip, vnodes[i].port);

	a = malloc(sizeof(struct args));
	a->node = n;
	a->nidx = me;
	if (pthread_create(&tt, NULL, test_thread, a))
		err(1, "pthread_create");

	dht_event_loop(n);
	errx(1, "should not reach here!");
	return 0;
}

int
main(int argc, char **argv)
{
	int i, status, fail = 0;
	pid_t pid;

	for (i = 0 ; i < N_NODES ; i++) {
		if (!fork()) {
			node(i);
			return 0;
		}
	}
	while (1) {
		pid = wait(&status);
		if (pid == -1) {
			if (errno == ECHILD)
				break;
			else
				err(1, "wait");
		} else {
			if (WIFEXITED(status) && WEXITSTATUS(status))
				fail++;
		}
	}
	sleep(5);
	return fail;
}
