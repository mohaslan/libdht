/*
 * Copyright (c) 2016 Mohamed Aslan <maslan@sce.carleton.ca>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <dht.h>
#include <hashtab.h>
#include <pthread.h>
#include <err.h>
#include <errno.h>
#include <time.h>
#include <unistd.h>
#include <sys/wait.h>

#define	N_REPLICAS	2

#define	MAX_KEYS	2
struct v_node {
	char *id;
	char *ip;
	int port;
	int n_keys;
	char *keys[MAX_KEYS];
	char *vals[MAX_KEYS];
};

#define	N_NODES	3
static struct v_node vnodes[] = {
	{"A", "127.0.0.1", 5000, 2,
	    {"hello", "world"},
	    {"world", "hello"}
	},
	{"B", "127.0.0.1", 5001, 2,
	    {"1hello", "world1"},
	    {"world", "hello"}
	},
	{"C", "127.0.0.1", 5002, 2,
	    {"12hello", "world12"},
	    {"world", "hello"}
	}
};

#define	MAX_RKEYS	5
struct result {
	int n;
	char *keys[MAX_RKEYS];
	char *vals[MAX_RKEYS];
	int visited[MAX_RKEYS];
};

static struct result results[] = {
	{
	    5,
	    {"hello", "1hello", "12hello", "world1", "world12"},
	    {"world", "world", "world", "hello", "hello"},
	    {0, 0, 0, 0, 0}
	},
	{
	    3,
	    {"1hello", "world", "world1"},
	    {"world", "hello", "hello"},
	    {0, 0, 0}
	},
	{
	    4,
	    {"hello", "12hello", "world", "world12"},
	    {"world", "world", "hello", "hello"},
	    {0, 0, 0, 0}
	}
};

struct args {
	int nidx;
	struct dht_node *node;
};


int
lfind(const char *key, const char *val, struct result *r)
{
	int i;

	for (i = 0 ; i < r->n ; i++) {
		if (!strcmp(key, r->keys[i]) && !strcmp(val, r->vals[i]) && !r->visited[i]) {
			r->visited[i] = 1;
			return 1;
		}
	}
	return 0;
}

void *
test_thread(void *arg)
{
	int i, fail = 0;
	int me = ((struct args *)arg)->nidx;
	struct dht_node *n = ((struct args *)arg)->node;
	struct hashtab *ht = (struct hashtab *)n->ht;
	struct timespec ts;
	char *key, *val;
	size_t key_size, val_size;
	size_t iter;

	while (!n->ready);

	sleep(5);
	(void) clock_gettime(CLOCK_REALTIME, &ts);
	for (i = 0 ; i < vnodes[me].n_keys ; i++) {
		if (!dht_put_tunable(n, vnodes[me].keys[i], vnodes[me].vals[i], &ts, N_REPLICAS))
			printf("FAILED [%s] dht_put_tunable.\n", n->id);
	}

	sleep(5);
	HASHTAB_FOREACH(*ht, iter, key, key_size, val, val_size) {
		if (lfind(key, val, &results[me])) {
			printf("OK\n");
		} else {
			fail++;
			printf("FAILED [%s] %s -> %s\n", n->id, key, val);
		}
	}

	_exit(fail);
	/* NOTREACHED */
	return NULL;
}

int
node(int me)
{
	int i;
	struct dht_node *n;
	struct args *a;
	pthread_t tp, tt;

	n = malloc(sizeof(struct dht_node));
	if (!dht_init(n, vnodes[me].id, vnodes[me].port, N_REPLICAS, 0, NULL))
		err(1, "dht_init");

	for (i = 0 ; i < N_NODES ; i++)
		if (i != me)
			dht_add_peer(n, vnodes[i].id, vnodes[i].ip, vnodes[i].port);

	a = malloc(sizeof(struct args));
	a->node = n;
	a->nidx = me;
	if (pthread_create(&tt, NULL, test_thread, a))
		err(1, "pthread_create");

	dht_event_loop(n);
	errx(1, "should not reach here!");
	return 0;
}

int
main(int argc, char **argv)
{
	int i, status, fail = 0;
	pid_t pid;

	for (i = 0 ; i < N_NODES ; i++) {
		if (!fork()) {
			node(i);
			return 0;
		}
	}
	while (1) {
		pid = wait(&status);
		if (pid == -1) {
			if (errno == ECHILD)
				break;
			else
				err(1, "wait");
		} else {
			if (WIFEXITED(status) && WEXITSTATUS(status))
				fail++;
		}
	}
	sleep(5);
	return fail;
}
