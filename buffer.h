/*
 * Copyright (c) 2016 Mohamed Aslan <maslan@sce.carleton.ca>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef BUFFER_H
#define BUFFER_H

#include <sys/types.h>

struct rbuffer {
	char	*rb_data;
	size_t	 rb_capacity;
	size_t	 rb_len;
	char	*rb_rp;
	char	*rb_wp;
	/* TODO: locking */
};

struct rbuffer *	rbuffer_new(size_t);
void			rbuffer_free(struct rbuffer *);
int			rbuffer_write(struct rbuffer *, const char *, size_t);
char *			rbuffer_read(struct rbuffer *, size_t);
char *			rbuffer_readline(struct rbuffer *);

#endif

