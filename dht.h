/*
 * Copyright (c) 2016 Mohamed Aslan <maslan@sce.carleton.ca>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef DHT_H
#define DHT_H

#include <time.h>
#include <sys/socket.h>

#define DHT_USEUDP	(1 << 0)


struct dht_node {
	char *id;
	uint64_t token;
	int port;
	int socket;
	int ev_queue;
	int n_replicas;
	int n_peers;
	int def_r;
	int def_w;
	uint8_t ready;
	uint32_t flags;
	struct sockaddr_storage saddr;
	struct dht_options *opts;
	struct dht_peer *peers;
	void *ht;
};

struct dht_peer {
	char *id;
	uint64_t token;
	int socket;
	uint64_t ready;
	time_t last_recv;
	struct sockaddr_storage s_addr;
};

struct dht_options {
	uint64_t (*partitioner)(const char *);
	int (*ht_init)(void **);
	int (*ht_put)(void *, const char *, const char *, struct timespec *);
	int (*ht_get)(void *, const char *, char **, struct timespec **);
	int (*ht_del)(void *, const char *);
};

int dht_init(struct dht_node *, const char *, int, int, uint32_t, struct dht_options *);
void dht_add_peer(struct dht_node *, const char *, const char *, int);
void dht_event_loop(struct dht_node *);
int dht_put_tunable(struct dht_node *, const char *, const char *, struct timespec *, int);
int dht_get_tunable(struct dht_node *, const char *, char **, int);
int dht_put(struct dht_node *, const char *, const char *, struct timespec *);
int dht_get(struct dht_node *, const char *, char **);

#endif
